# php

Slim php Docker Images built with best practices in mind and used in JUIT projects.

The images are based on debian slim.

## How to use this image

### tagging strategy

We only create docker tags by php minor and exact version. Changes in xdebug version or added/updated
php extensions are not reflected in the tags, only in the sha256 hash of the images.

### enable extensions on container startup

By utilizing the environment variable `PHP_EXTENSIONS` one can enable several php extensions which
are already pre-built. If the environment variable is not set, only `opcache` will be enabled by default.

### php extensions (which are baked into php runtime)

The following extensions are baked right into php. The reason mainly is that tools like composer require
several of them to work properly and many projects require them as well, so it makes no sense to have them
optional.

- argon2
- curl
- ftp
- libedit
- mbstring
- mhash
- openssl
- zlib

### pre-built php extensions (which can be enabled)

- ast
- bcmath
- calendar
- ffi
- intl
- mysqli
- opcache
- pdo_mysql
- pdo_pgsql
- pdo_sqlite
- pgsql
- phar
- simplexml
- spx
- soap
- sodium
- sqlite3
- xdebug
- xhprof
- xsl
- zip

### docker compose

```yml
# docker-compose.yml
version: '3.8'

services:
  php:
    image: juitde/php:$PHP_VERSION
    environment:
      - "PHP_EXTENSIONS=opcache intl pgsql pdo_pgsql xdebug"
```

## What's inside?

There are several images built which all are based on debian-slim base images (Debian Buster right now).
Currently we build PHP 7.4, PHP 8.0 and PHP 8.1 (pre-release versions).

PHP is always compiled from source (in the same way it's done in the official php docker images).
Additional php extensions are built but not enabled by default so it's up to the user to enable them
at will.

We build only fpm images (but it includes the cli too).

The following variants are built:

- fpm: contains only php
- wkhtmltopdf: contains additionally wkhtmltopdf, chromium and chromedriver (can be used with https://github.com/symfony/panther)

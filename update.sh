#!/usr/bin/env bash
set -Eeuo pipefail

# https://www.php.net/gpg-keys.php
declare -A gpgKeys=(
	# https://wiki.php.net/todo/php84
	# eric & calvinb & saki
	# https://www.php.net/gpg-keys.php#gpg-8.4
	[8.4]='AFD8691FDAEDF03BDF6E460563F15A9B715376CA 9D7F99A0CB8F05C8A6958D6256A97AF7600A39A6 0616E93D95AF471243E26761770426E17EBBB3DD'

	# https://wiki.php.net/todo/php83
	# pierrick & bukka & eric
	# https://www.php.net/gpg-keys.php#gpg-8.3
	[8.3]='1198C0117593497A5EC5C199286AF1F9897469DC AFD8691FDAEDF03BDF6E460563F15A9B715376CA C28D937575603EB4ABB725861C0779DC5C0A9DE4'

	# https://wiki.php.net/todo/php82
	# ramsey & pierrick & sergey
	# https://www.php.net/gpg-keys.php#gpg-8.2
	[8.2]='1198C0117593497A5EC5C199286AF1F9897469DC 39B641343D8C104B2B146DC3F9C39DC0B9698544 E60913E4DF209907D8E30D96659A97C9CF2A795A'

	# https://wiki.php.net/todo/php81
	# krakjoe & ramsey & patrickallaert
	# https://www.php.net/gpg-keys.php#gpg-8.1
	[8.1]='528995BFEDFBA7191D46839EF9BA0ADA31CBD89E 39B641343D8C104B2B146DC3F9C39DC0B9698544 F1F692238FBC1666E5A5CCD4199F9DFEF6FFBAFD'

	# https://wiki.php.net/todo/php80
	# pollita & carusogabriel
	# https://www.php.net/gpg-keys.php#gpg-8.0
	[8.0]='1729F83938DA44E27BA0F4D3DBDB397470D12172 BFDDD28642824F8118EF77909B67A5C12229118F 2C16C765DBE54A088130F1BC4B9B5F600B55F3B4 39B641343D8C104B2B146DC3F9C39DC0B9698544'

	# https://wiki.php.net/todo/php74
	# petk & derick
	# https://www.php.net/gpg-keys.php#gpg-7.4
	[7.4]='42670A7FE4D0441C8E4632349E4FDC074A4EF02D 5A52880781F755608BF815FC910DEB46F53EA312'
)
# see https://www.php.net/downloads.php

declare -A xdebugMinVersions=(
	[8.4]='3.4.0'
	[8.3]='3.4.0'
	[8.2]='3.4.0'
	[8.1]='3.4.0'
	[8.0]='3.4.0'
	[7.4]='3.1.6'
)

declare -A spxMinVersions=(
	[8.4]='0.4.17'
	[8.3]='0.4.14'
	[8.2]='0.4.14'
	[8.1]='0.4.14'
	[8.0]='0.4.14'
	[7.4]='0.4.14'
)

declare -A xhprofMinVersions=(
	[8.4]='2.3.10'
	[8.3]='2.3.9'
	[8.2]='2.3.9'
	[8.1]='2.3.9'
	[8.0]=''
	[7.4]=''
)

declare -A pcovMinVersions=(
	[8.4]=''
	[8.3]='1.0.10'
	[8.2]='1.0.10'
	[8.1]='1.0.10'
	[8.0]='1.0.10'
	[7.4]='1.0.10'
)

declare -A buildModes=(
	[8.4-fpm]='on_success'
	[8.4-wkhtmltopdf]='on_success'
	[8.3-fpm]='on_success'
	[8.3-wkhtmltopdf]='on_success'
	[8.2-fpm]='on_success'
	[8.2-wkhtmltopdf]='on_success'
	[8.1-fpm]='on_success'
	[8.1-wkhtmltopdf]='on_success'
	[8.0-fpm]='manual'
	[8.0-wkhtmltopdf]='manual'
	[7.4-fpm]='manual'
	[7.4-wkhtmltopdf]='manual'
)

cd "$(dirname "$(readlink -f "$BASH_SOURCE")")/specs"

versions=( "$@" )
if [ ${#versions[@]} -eq 0 ]; then
	versions=( */ )
fi
versions=( "${versions[@]%/}" )

suites=( "buster" "bullseye" "bookworm" )
variants=( "fpm" "wkhtmltopdf" )

generated_warning() {
	cat <<-EOH
		#
		# NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
		#
		# PLEASE DO NOT EDIT IT DIRECTLY.
		#

	EOH
}

gitlabSteps=
for version in "${versions[@]}"; do
	rcVersion="${version%-rc}"
	isRc=0

	# "7", "5", etc
	majorVersion="${rcVersion%%.*}"
	# "2", "1", "6", etc
	minorVersion="${rcVersion#$majorVersion.}"
	minorVersion="${minorVersion%%.*}"

	xdebug_version=${xdebugMinVersions[$rcVersion]}
	spx_version=${spxMinVersions[$rcVersion]}
	xhprof_version=${xhprofMinVersions[$rcVersion]}
	pcov_version=${pcovMinVersions[$rcVersion]}

	# scrape the relevant API based on whether we're looking for pre-releases
	apiUrl="https://www.php.net/releases/index.php?json&max=100&version=${rcVersion%%.*}"
	apiJqExpr='
		(keys[] | select(startswith("'"$rcVersion"'."))) as $version
		| [ $version, (
			.[$version].source[]
			| select(.filename | endswith(".xz"))
			|
				"https://www.php.net/distributions/" + .filename,
				"https://www.php.net/distributions/" + .filename + ".asc",
				.sha256 // ""
		) ]
	'
	if [ "$rcVersion" != "$version" ]; then
		isRc=1
		apiUrl='https://qa.php.net/api.php?type=qa-releases&format=json'
		apiJqExpr='
			.releases[]
			| select(.version | startswith("'"$rcVersion"'."))
			| [
				.version,
				.files.xz.path // "",
				"",
				.files.xz.sha256 // ""
			]
		'
	fi
	IFS=$'\n'
	possibles=( $(
		curl -fsSL "$apiUrl" \
			| jq --raw-output "$apiJqExpr | @sh" \
			| sort -rV
	) )
	unset IFS

	if [ "${#possibles[@]}" -eq 0 ]; then
		echo >&2
		echo >&2 "error: unable to determine available releases of $version"
		echo >&2
		exit 1
	fi

	# format of "possibles" array entries is "VERSION URL.TAR.XZ URL.TAR.XZ.ASC SHA256" (each value shell quoted)
	#   see the "apiJqExpr" values above for more details
	eval "possi=( ${possibles[0]} )"
	fullVersion="${possi[0]}"
	url="${possi[1]}"
	ascUrl="${possi[2]}"
	sha256="${possi[3]}"

	gpgKey="${gpgKeys[$rcVersion]}"
	if [ -z "$gpgKey" ]; then
		echo >&2 "ERROR: missing GPG key fingerprint for $version"
		echo >&2 "  try looking on https://www.php.net/downloads.php#gpg-$version"
		exit 1
	fi

	# if we don't have a .asc URL, let's see if we can figure one out :)
	if [ -z "$ascUrl" ] && wget -q --spider "$url.asc"; then
		ascUrl="$url.asc"
	fi

	dockerfiles=()

	for suite in "${suites[@]}"; do
		[ -d "$version/$suite" ] || continue
		alpineVer="${suite#alpine}"

		baseDockerfile=Dockerfile-debian.template
		if [ "${suite#alpine}" != "$suite" ]; then
			baseDockerfile=Dockerfile-alpine.template
		fi

		for variant in "${variants[@]}"; do
			[ -d "$version/$suite/$variant" ] || continue
			{ generated_warning; cat "../templates/$baseDockerfile"; } > "$version/$suite/$variant/Dockerfile"

			buildMode="${buildModes[$rcVersion-$variant]}"

			echo "Generating $version/$suite/$variant/Dockerfile from $baseDockerfile + $variant-Dockerfile-block-*"
			gawk -i inplace -v variant="$variant" '
				$1 == "##</autogenerated>##" { ia = 0 }
				!ia { print }
				$1 == "##<autogenerated>##" { ia = 1; ab++; ac = 0; if (system("test -f ../templates/" variant "-Dockerfile-block-" ab) != 0) { ia = 0 } }
				ia { ac++ }
				ia && ac == 1 { system("cat ../templates/" variant "-Dockerfile-block-" ab) }
			' "$version/$suite/$variant/Dockerfile"

			echo -n "$fullVersion" > "$version/$suite/$variant/VERSION"
			cp -a \
				../templates/docker-php-entrypoint \
				../templates/docker-php-ext-* \
				../templates/docker-php-source \
				"$version/$suite/$variant/"
			if [ "$variant" = 'apache' ]; then
				cp -a apache2-foreground "$version/$suite/$variant/"
			fi
			if [ "$majorVersion" = '7' -a "$minorVersion" -lt '2' ]; then
				# argon2 password hashing is only supported in 7.2+
				sed -r \
					-e '/##<argon2-stretch>##/,/##<\/argon2-stretch>##/d' \
					-e '/argon2/d' \
					"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			elif [ "$suite" != 'stretch' ]; then
				# and buster+ doesn't need to pull argon2 from stretch-backports
				sed -r \
					-e '/##<argon2-stretch>##/,/##<\/argon2-stretch>##/d' \
					"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			fi
			if [ "$majorVersion" = '7' -a "$minorVersion" -lt '4' ]; then
				# oniguruma is part of mbstring in php 7.4+
				sed -r \
					-e '/oniguruma-dev|libonig-dev/d' \
					"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			fi
			if [ "$majorVersion" -ge '8' ]; then
				# 8 and above no longer include pecl/pear (see https://github.com/docker-library/php/issues/846#issuecomment-505638494)
				sed -r \
					-e '/pear |pearrc|pecl.*channel/d' \
					"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			fi
			if [ "$majorVersion" != '7' ] || [ "$minorVersion" -lt '4' ]; then
				# --with-pear is only relevant on PHP 7, and specifically only 7.4+ (see https://github.com/docker-library/php/issues/846#issuecomment-505638494)
				sed -r \
					-e '/--with-pear/d' \
					"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			fi
			if [ "$majorVersion" = '7' -a "$minorVersion" -lt '2' ]; then
				# sodium is part of php core 7.2+ https://wiki.php.net/rfc/libsodium
				sed -r '/sodium/d' "$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			fi
			if [ "$variant" = 'fpm' -a "$majorVersion" = '7' -a "$minorVersion" -lt '3' ]; then
				# php-fpm "decorate_workers_output" is only available in 7.3+
				sed -r \
					-e '/decorate_workers_output/d' \
					-e '/log_limit/d' \
					"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			fi
			if [ "$suite" = 'stretch' ] || [ "$majorVersion" -gt '7' ] || { [ "$majorVersion" = '7' ] && [ "$minorVersion" -ge '4' ]; }; then
				# https://github.com/docker-library/php/issues/865
				# https://bugs.php.net/bug.php?id=76324
				# https://github.com/php/php-src/pull/3632
				# https://github.com/php/php-src/commit/2d03197749696ac3f8effba6b7977b0d8729fef3
				sed -r \
					-e '/freetype-config/d' \
					"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			fi
			if [[ "$suite" == alpine* ]] && [ "$majorVersion" = '7' ] && [ "$minorVersion" -lt '4' ]; then
				# https://github.com/docker-library/php/issues/888
				sed -r \
					-e '/linux-headers/d' \
					"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			fi
			if [ "$majorVersion" -lt '8' ]; then
				# https://github.com/php/php-src/commit/161adfff3f437bf9370e037a9e2bf593c784ccff
				sed -r \
					-e 's/--enable-zts/--enable-maintainer-zts/g' \
					"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
				mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			fi

			# remove any _extra_ blank lines created by the deletions above
			gawk '
				{
					if (NF == 0 || (NF == 1 && $1 == "\\")) {
						blank++
					}
					else {
						blank = 0
					}

					if (blank < 2) {
						print
					}
				}
			' "$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
			mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"

			sed -r \
				-e 's!%%DEBIAN_TAG%%!'"$suite-slim"'!' \
				-e 's!%%DEBIAN_SUITE%%!'"$suite"'!' \
				-e 's!%%ALPINE_VERSION%%!'"$alpineVer"'!' \
				-e 's!%%PHP_VERSION%%!'"$fullVersion"'!' \
				-e 's!%%GPG_KEYS%%!'"$gpgKey"'!' \
				-e 's!%%PHP_URL%%!'"$url"'!' \
				-e 's!%%PHP_ASC_URL%%!'"$ascUrl"'!' \
				-e 's!%%PHP_SHA256%%!'"$sha256"'!' \
				-e 's!%%XDEBUG_VERSION%%!'"$xdebug_version"'!' \
				-e 's!%%SPX_VERSION%%!'"$spx_version"'!' \
				-e 's!%%PCOV_VERSION%%!'"$pcov_version"'!' \
				-e 's!%%XHPROF_VERSION%%!'"$xhprof_version"'!' \
				"$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
			mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"
			dockerfiles+=( "$version/$suite/$variant/Dockerfile" )
		done
	done

	# update entrypoint commands
	for dockerfile in "${dockerfiles[@]}"; do
		cmd="$(awk '$1 == "CMD" { $1 = ""; print }' "$dockerfile" | tail -1 | jq --raw-output '.[0]')"
		entrypoint="$(dirname "$dockerfile")/docker-php-entrypoint"
		sed 's! php ! '"$cmd"' !g' "$entrypoint" > "$entrypoint.tmp"
		mv "$entrypoint.tmp" "$entrypoint"
		chmod +x "$entrypoint"
	done

	echo "Docker files: " "${dockerfiles[@]}"
	newTravisEnv=
	for dockerfile in "${dockerfiles[@]}"; do
		dir="${dockerfile%Dockerfile}"
		dir="${dir%/}"
		variant="${dir#$version}"
		variant="${variant#/}"
		newGitlabStep="\nbuild $version $variant:\n    extends:\n        - .docker-image-build\n    when: $buildMode\n    variables:\n        VERSION: '$version'\n        VARIANT: '$variant'"
		if [ "$isRc" == "1" ]; then
			newGitlabStep="$newGitlabStep\n    allow_failure: true"
		fi
		gitlabSteps="$gitlabSteps$newGitlabStep"
	done
done

gitlabCi=$(echo "$gitlabSteps" | awk '{gsub(/\\n/, "\n")}1')
echo "$gitlabCi" > ../.gitlab/docker-images.yml
